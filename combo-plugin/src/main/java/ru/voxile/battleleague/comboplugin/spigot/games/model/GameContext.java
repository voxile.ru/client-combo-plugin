package ru.voxile.battleleague.comboplugin.spigot.games.model;

import lombok.Data;
import ru.voxile.battleleague.comboplugin.api.common.RemoteServerAddress;
import ru.voxile.battleleague.comboplugin.api.common.RoomStatus;

import java.util.Collection;
import java.util.UUID;

@Data
public class GameContext {

    private Area area;

    private Collection<UUID> players;

    private String roomId;

    private RoomStatus roomStatus;

    private RemoteServerAddress address;
}
