package ru.voxile.battleleague.comboplugin.spigot.http;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.RequiredArgsConstructor;
import org.bukkit.OfflinePlayer;
import ru.voxile.battleleague.comboplugin.api.play.ProjectPlayerDto;

@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class PlayerMapper {

    public ProjectPlayerDto toProjectPlayerDto(OfflinePlayer player) {
        return ProjectPlayerDto.builder()
                .playerUuid(player.getUniqueId())
                .playerName(player.getName())
                .build();
    }
}
