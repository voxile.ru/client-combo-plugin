package ru.voxile.battleleague.comboplugin.common.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Builder;
import lombok.Value;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import ru.voxile.battleleague.comboplugin.common.Permissions;

import java.util.Collection;
import java.util.UUID;

@Value
@Builder
public class GameServerInfo {

    @NotNull
    String areaName;

    @NotNull
    String host;

    @Positive @Min(1) @Max(65535)
    int port;

    @NotNull
    Collection<UUID> playerIds;

    @Builder.Default
    @SuppressWarnings("FieldMayBeStatic")
    boolean forceGloballyUniqueName = true;

    @Builder.Default
    @SuppressWarnings("FieldMayBeStatic")
    boolean restricted = true;

    public String getPermission() {
        return restricted ?
                "bungeecord.server." + toUniqueName()
                : Permissions.SINGLE_GLOBAL_PERMISSION;
    }

    /**
     * Обязательно должно быть достаточно уникальным!
     */
    public String toUniqueName() {
        if (forceGloballyUniqueName) {
            String concatenated = String.format("%s@%s:%d", areaName, host, port);
            byte[] hashedSha256 = DigestUtils.sha256(concatenated);
            return Base64.encodeBase64String(hashedSha256);
        }

        return areaName;
    }
}
