package ru.voxile.battleleague.comboplugin.common;

import com.google.gson.Gson;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

@UtilityClass
public class JsonUtils {

    private final Gson GSON = BeanFactory.gson();

    @SneakyThrows
    public <T> T read(byte[] message, Class<T> clazz) {
        try (ByteArrayInputStream bais = new ByteArrayInputStream(message);
             InputStreamReader isr = new InputStreamReader(bais, StandardCharsets.UTF_8)) {
            return GSON.fromJson(isr, clazz);
        }
    }

    public byte[] write(Object payload) {
        return GSON.toJson(payload)
                .getBytes(StandardCharsets.UTF_8);
    }
}
