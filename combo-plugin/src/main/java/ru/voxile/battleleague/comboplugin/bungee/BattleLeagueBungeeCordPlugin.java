package ru.voxile.battleleague.comboplugin.bungee;

import com.google.inject.Inject;
import jakarta.validation.constraints.NotNull;
import lombok.NonNull;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.scheduler.TaskScheduler;
import one.util.streamex.StreamEx;
import ru.voxile.battleleague.comboplugin.bungee.di.BungeeCordBinderModule;
import ru.voxile.battleleague.comboplugin.bungee.implementation.AdminMessenger;
import ru.voxile.battleleague.comboplugin.bungee.implementation.BungeeCordMessageListener;
import ru.voxile.battleleague.comboplugin.bungee.implementation.ServersContainer;
import ru.voxile.battleleague.comboplugin.common.SpigotBungeeMessaging;
import ru.voxile.battleleague.comboplugin.common.dto.GameServerInfo;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * https://www.spigotmc.org/wiki/bungeecord-plugin-development/
 */
@SuppressWarnings("unused")
public final class BattleLeagueBungeeCordPlugin extends Plugin {

    @Inject
    private BungeeCordMessageListener bungeeCordMessageListener;
    @Inject
    private ServersContainer serversContainer;
    @Inject
    private AdminMessenger adminMessenger;

    private ServerInfo lobbyServer;

    @Override
    public void onEnable() {
        // Dependency Injection.
        new BungeeCordBinderModule(this)
                .injectFields();

        ProxyServer proxy = getProxy();
        proxy.registerChannel(SpigotBungeeMessaging.CHANNEL_ID);
        proxy.getPluginManager().registerListener(this, bungeeCordMessageListener);
    }

    @Override
    public void onDisable() {
        getProxy().getPluginManager().unregisterListeners(this);
    }

    public void updateLobbyServer(ServerInfo serverInfo) {
        if (lobbyServer == null) {
            lobbyServer = serverInfo;
            getLogger().info("Stored lobby server: " + serverInfo.getName());
        }
    }

    public void onAddServer(@NonNull GameServerInfo gameServerInfo) {
        // Попытались зарегистрировать новый сервер.
        boolean isNewServer = serversContainer.isRegisteredNew(gameServerInfo);

        if (isNewServer) {
            // Отправили игроков играть на нём.
            ServerInfo serverInfo = serversContainer.getRegistration(gameServerInfo)
                    .orElseThrow(() -> new IllegalStateException("Sorry, this is my fault."));
            teleportPlayersToGame(gameServerInfo, serverInfo);
        } else {
            // TODO HERE: Внести корректировки в состав игроков?
        }
    }

    public void teleportPlayersToGame(
            @NonNull GameServerInfo gameServerInfo,
            @NonNull ServerInfo serverInfo
    ) {
        ProxyServer proxy = getProxy();
        TaskScheduler scheduler = proxy.getScheduler();
        Collection<UUID> playersToTeleport = gameServerInfo.getPlayerIds();

        int counter = 1;
        for (UUID playerId : playersToTeleport) {
            scheduler.schedule(this,
                    () -> Optional.of(playerId)
                            .map(proxy::getPlayer)
                            .ifPresent(p -> sendPlayerToGameInstance(p, serverInfo)),
                    counter++, TimeUnit.SECONDS);
        }
    }

    public void onRemoveServer(@NonNull GameServerInfo gameServerInfo) {
        if (lobbyServer == null) {
            return;
        }

        serversContainer.getRegistration(gameServerInfo)
                .ifPresent(serverInfo -> {
                    // Отправили всех игроков обратно в лобби.
                    CompletableFuture<?>[] completableFutures = returnAllPlayersToLobby(gameServerInfo, serverInfo);

                    TaskScheduler scheduler = getProxy().getScheduler();
                    scheduler.runAsync(
                            this,
                            () -> forgetGameServer(completableFutures, gameServerInfo, serverInfo));
                });
    }

    private void forgetGameServer(
            @NotNull CompletableFuture<?>[] completableFutures,
            @NotNull GameServerInfo gameServerInfo,
            @NotNull ServerInfo serverInfo
    ) {
        try {
            // Дожидаемся, пока они все туда переберутся.
            CompletableFuture
                    .allOf(completableFutures)
                    .get(1, TimeUnit.MINUTES);
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            getProxy().getLogger().warning("Exception in forgetGameServer(): " + ex.getMessage());
        }

        // Удалили из списка игр.
        serversContainer.unregister(gameServerInfo);
    }

    private CompletableFuture<?>[] returnAllPlayersToLobby(@NotNull GameServerInfo gameServerInfo, ServerInfo serverInfo) {
        ProxyServer proxy = getProxy();
        return StreamEx.of(gameServerInfo.getPlayerIds())
                .map(proxy::getPlayer)
                .nonNull()
                .map(p -> returnPlayerBackToLobby(p, serverInfo))
                .toArray(CompletableFuture.class);
    }

    private static Callback<Boolean> playerToCallback(ProxiedPlayer player, CompletableFuture<Void> cf) {
        return (Boolean result, Throwable error) -> cf.complete(null);
    }

    public void onPlayerConnected(@NonNull ProxiedPlayer player) {
        // Если игрок считается играющим на каком-то сервере, отправляем его туда.
        serversContainer.findGameForPlayer(player.getUniqueId())
                .ifPresent(serverInfo -> sendPlayerToGameInstance(player, serverInfo));
    }

    public void onServerKick(@NonNull ProxiedPlayer player, @NonNull ServerInfo serverInfo) {
        // Игровой сервер выгнал игрока.
        getLogger().fine(String.format("Player '%s' kicked from server '%s'.", player.getName(), serverInfo.getName()));
        // Больше он его не впустит, можно перемещать в лобби.
        returnPlayerBackToLobby(player, serverInfo);
    }

    public void onServerDisconnect(@NonNull ProxiedPlayer player, @NonNull ServerInfo serverInfo) {
        // Игровой сервер выключается.
        getLogger().fine(String.format("Server '%s' disconnected.", serverInfo.getName()));
        // Нам нечего больше делать, кроме как переместить игрока в лобби.
        returnPlayerBackToLobby(player, serverInfo);
    }

    private void sendPlayerToGameInstance(
            @NonNull ProxiedPlayer player,
            @NonNull ServerInfo serverInfo
    ) {
        if (serverInfo.isRestricted()) {
            player.setPermission("bungeecord.server." + serverInfo.getName(), true);
        }
        player.connect(serverInfo, ServerConnectEvent.Reason.PLUGIN);

        getLogger().fine(String.format("Player '%s' moved to server '%s'.", player.getName(), serverInfo.getName()));
    }

    private CompletableFuture<Void> returnPlayerBackToLobby(
            @NonNull ProxiedPlayer player,
            @NonNull ServerInfo serverInfo
    ) {
        CompletableFuture<Void> completableFuture = new CompletableFuture<>();
        Callback<Boolean> callback = (Boolean result, Throwable error) -> completableFuture.complete(null);

        player.connect(lobbyServer, callback, ServerConnectEvent.Reason.PLUGIN);
        if (serverInfo.isRestricted()) {
            player.setPermission("bungeecord.server." + serverInfo.getName(), false);
        }

        getLogger().fine(String.format("Player '%s' moved back to lobby.", player.getName()));

        return completableFuture;
    }
}
