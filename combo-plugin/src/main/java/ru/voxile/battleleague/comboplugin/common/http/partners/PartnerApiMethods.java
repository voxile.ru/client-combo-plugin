package ru.voxile.battleleague.comboplugin.common.http.partners;

import lombok.experimental.UtilityClass;

@UtilityClass
class PartnerApiMethods {

    /**
     * Отправляется на сервер, когда игроки входят/выходят
     * в одну из зон ожидания в лобби.
     * В ответ сервер сообщает что нужно делать, например, начать телепортацию игроков.
     */
    final String ROOM_PLAYERS = "/api/play/start";

    /**
     * Регулярный запрос на сервер для получения опционального сообщения для
     * отображения игрокам. Примеры сообщений:
     * <ul>
     *     <li>Собрана команда противников, есть возможность начать новую игру.</li>
     *     <li>Игра в зоне XXX скоро завершится.</li>
     *     <li>В зоне XXX запланирован глобальный эвент на 16:00 по Мск.</li>
     * </ul>
     */
    final String GET_MESSAGE = "/api/play/messages/next";
}
