package ru.voxile.battleleague.comboplugin.spigot.games;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.RequiredArgsConstructor;
import one.util.streamex.StreamEx;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.Plugin;
import ru.voxile.battleleague.comboplugin.spigot.games.model.Area;
import ru.voxile.battleleague.comboplugin.spigot.messaging.PluginLogger;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class AreaService {

    public static final String AREAS_SECTION = "lobby-areas";

    private final Plugin plugin;
    private final PluginLogger logger;

    private final Map<String, Area> areasByName = new ConcurrentHashMap<>();

    public void reloadFromConfig() {
        ConfigurationSection areasSection = plugin.getConfig()
                .getConfigurationSection(AREAS_SECTION);
        Set<String> areaNames = Objects.requireNonNull(areasSection)
                .getKeys(false);
        String areasNameList = StreamEx.of(areaNames).joining(", ");
        logger.info("Found areas in config: %s.", areasNameList);

        areasByName.clear();
        StreamEx.of(areaNames)
                .mapToEntry(areasSection::getConfigurationSection)
                .nonNullValues()
                .mapToValue(this::fromConfig)
                .nonNullValues()
                .peekKeys(an -> logger.info("Restored area: %s.", an))
                .forKeyValue(areasByName::put);
    }

    public Area register(String name, Location p1, Location p2) {
        Area area = Area.create(name, p1, p2);
        if (area != null) {
            areasByName.put(area.getName(), area);
            updateAreasInConfig();
            return area;
        }

        return null;
    }

    public Collection<Area> getAll() {
        return areasByName.values();
    }

    public Area getByName(String name) {
        return areasByName.get(name);
    }

    public void deleteArea(Area area) {
        if (areasByName.remove(area.getName()) != null) {
            updateAreasInConfig();
        }
    }

    private void updateAreasInConfig() {
        ConfigurationSection areasSection = plugin.getConfig()
                .createSection(AREAS_SECTION);
        for (Area area : areasByName.values()) {
            ConfigurationSection section = areasSection.createSection(area.getName());
            Location p1 = area.getMin();
            Location p2 = area.getMax();
            String worldName = p1.getWorld().getName();
            section.set("world", worldName);
            section.set("point-min.x", p1.getBlockX());
            section.set("point-min.y", p1.getBlockY());
            section.set("point-min.z", p1.getBlockZ());
            section.set("point-max.x", p2.getBlockX());
            section.set("point-max.y", p2.getBlockY());
            section.set("point-max.z", p2.getBlockZ());
            section.createSection("properties", area.getProperties());
        }
        plugin.saveConfig();
    }

    private Area fromConfig(String name, ConfigurationSection areaSection) {
        String defaultWorld = Bukkit.getWorlds()
                .get(0)
                .getName();
        String worldName = areaSection.getString("world", defaultWorld);
        World world = Bukkit.getWorld(worldName);
        if (world == null) {
            // Указан несуществующий мир.
            logger.warn("Area's '%s' world '%s' is unknown", name, worldName);
            return null;
        }

        boolean incorrectCoordinates = !StreamEx.of(
                        "point-min.x", "point-min.y", "point-min.z",
                        "point-max.x", "point-max.y", "point-max.z")
                .allMatch(s -> areaSection.isInt(s) || areaSection.isLong(s));
        if (incorrectCoordinates) {
            // Не заданы все необходимые координаты.
            logger.warn("Area's '%s' coordinates are incorrect", name);
            return null;
        }

        long x1 = areaSection.getLong("point-min.x");
        long y1 = areaSection.getLong("point-min.y");
        long z1 = areaSection.getLong("point-min.z");
        long x2 = areaSection.getLong("point-max.x");
        long y2 = areaSection.getLong("point-max.y");
        long z2 = areaSection.getLong("point-max.z");
        Location p1 = new Location(world, x1, y1, z1);
        Location p2 = new Location(world, x2, y2, z2);

        Map<String, String> properties = extractAreaProperties(areaSection);

        return Area.create(name, p1, p2, properties);
    }

    private static Map<String, String> extractAreaProperties(ConfigurationSection areaSection) {
        ConfigurationSection propertiesSection = areaSection.getConfigurationSection("properties");
        if (propertiesSection == null) {
            return new HashMap<>();
        }

        Set<String> keys = propertiesSection.getKeys(false);

        return StreamEx.of(keys)
                .mapToEntry(propertiesSection::getString)
                .filterValues(value -> value != null && !value.isEmpty())
                .toCustomMap(HashMap::new);
    }
}
