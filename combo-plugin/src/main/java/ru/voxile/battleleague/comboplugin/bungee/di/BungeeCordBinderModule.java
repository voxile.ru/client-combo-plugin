package ru.voxile.battleleague.comboplugin.bungee.di;

import com.google.gson.Gson;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import net.md_5.bungee.api.plugin.Plugin;
import okhttp3.OkHttpClient;
import ru.voxile.battleleague.comboplugin.bungee.BattleLeagueBungeeCordPlugin;
import ru.voxile.battleleague.comboplugin.common.BeanFactory;

public class BungeeCordBinderModule extends AbstractModule {

    private final BattleLeagueBungeeCordPlugin plugin;
    private final Injector injector;

    public BungeeCordBinderModule(BattleLeagueBungeeCordPlugin plugin) {
        this.plugin = plugin;
        //noinspection ThisEscapedInObjectConstruction
        this.injector = Guice.createInjector(this);
    }

    public void injectFields() {
        injector.injectMembers(plugin);
    }

    @Override
    protected void configure() {
        // Экземпляр плагина.
        bind(Plugin.class).toInstance(plugin);
        bind(BattleLeagueBungeeCordPlugin.class).toInstance(plugin);

        // Утилиты.
        bind(Gson.class).toInstance(BeanFactory.gson());
        bind(OkHttpClient.class).toInstance(BeanFactory.httpClient());
    }
}
