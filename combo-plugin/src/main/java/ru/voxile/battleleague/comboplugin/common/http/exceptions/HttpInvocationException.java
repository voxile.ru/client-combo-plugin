package ru.voxile.battleleague.comboplugin.common.http.exceptions;

import lombok.Builder;

public final class HttpInvocationException extends RuntimeException {

    private static final long serialVersionUID = 739540292991875414L;

    @Builder
    private HttpInvocationException(
            String message,
            Throwable cause
    ) {
        super(message, cause);
    }
}
