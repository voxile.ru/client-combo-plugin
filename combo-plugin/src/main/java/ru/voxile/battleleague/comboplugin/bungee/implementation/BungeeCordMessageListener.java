package ru.voxile.battleleague.comboplugin.bungee.implementation;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ServerDisconnectEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;
import ru.voxile.battleleague.comboplugin.bungee.BattleLeagueBungeeCordPlugin;
import ru.voxile.battleleague.comboplugin.common.JsonUtils;
import ru.voxile.battleleague.comboplugin.common.SpigotBungeeMessaging;
import ru.voxile.battleleague.comboplugin.common.dto.BungeeCommandDto;
import ru.voxile.battleleague.comboplugin.common.dto.GameServerInfo;

@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class BungeeCordMessageListener implements Listener {

    private final BattleLeagueBungeeCordPlugin plugin;
    private final ServersContainer serversContainer;

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPluginMessage(PluginMessageEvent event) {
        // Принимаем сообщения только по выделенному каналу.
        if (!SpigotBungeeMessaging.CHANNEL_ID.equals(event.getTag())) {
            return;
        }

        // Только сообщения, отправленные Spigot-сервером.
        if (!(event.getSender() instanceof Server)) {
            return;
        }

        event.setCancelled(true);

        ServerInfo lobbyServer = ((Server) event.getSender()).getInfo();
        plugin.updateLobbyServer(lobbyServer);

        byte[] message = event.getData();
        BungeeCommandDto commandDto = JsonUtils.read(message, BungeeCommandDto.class);

        GameServerInfo gameServerInfo = commandDto.getServerInfo();
        switch (commandDto.getAction()) {
            case ADD_SERVER:
                plugin.onAddServer(gameServerInfo);
                break;
            case REMOVE_SERVER:
                plugin.onRemoveServer(gameServerInfo);
                break;
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerConnected(PostLoginEvent event) {
        plugin.onPlayerConnected(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onServerKick(ServerKickEvent event) {
        String serverName = event.getKickedFrom().getName();
        serversContainer.getRegistration(serverName)
                .ifPresent(si -> {
                    plugin.onServerKick(event.getPlayer(), si);
                    event.setCancelled(true);
                });
    }

    /**
     * FIXME: Исследование.
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void onServerDisconnect(ServerDisconnectEvent event) {
        String serverName = event.getTarget().getName();
        serversContainer.getRegistration(serverName)
                .ifPresent(si -> plugin.onServerDisconnect(event.getPlayer(), si));
    }
}
