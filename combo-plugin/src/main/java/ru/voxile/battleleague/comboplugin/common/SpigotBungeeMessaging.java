package ru.voxile.battleleague.comboplugin.common;

import lombok.experimental.UtilityClass;

@UtilityClass
public class SpigotBungeeMessaging {

    public static final String CHANNEL_ID = "battle-league:combo-plugin-channel";
}
