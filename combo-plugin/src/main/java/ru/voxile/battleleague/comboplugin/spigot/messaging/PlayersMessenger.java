package ru.voxile.battleleague.comboplugin.spigot.messaging;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.RequiredArgsConstructor;
import one.util.streamex.StreamEx;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import ru.voxile.battleleague.comboplugin.common.Permissions;

import java.util.Collection;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class PlayersMessenger {

    private final Server server;

    public void broadcastToAll(String message) {
        StreamEx.of(server.getOnlinePlayers())
                .forEach(p -> p.sendMessage(message));
    }

    public void messageToAdmins(String message) {
        StreamEx.of(server.getOnlinePlayers())
                .filter(p -> p.hasPermission(Permissions.SINGLE_GLOBAL_PERMISSION))
                .forEach(p -> p.sendMessage(message));
    }

    public void messageToPlayers(Collection<UUID> players, String message) {
        StreamEx.of(players)
                .map(server::getOfflinePlayer)
                .filter(OfflinePlayer::isOnline)
                .select(Player.class)
                .forEach(p -> p.sendMessage(message));
    }
}
