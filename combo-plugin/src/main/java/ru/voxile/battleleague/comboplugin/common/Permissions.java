package ru.voxile.battleleague.comboplugin.common;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Permissions {

    public static final String SINGLE_GLOBAL_PERMISSION = "battleleague.admin";
}
