package ru.voxile.battleleague.comboplugin.spigot.di;

import com.google.gson.Gson;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import lombok.RequiredArgsConstructor;
import okhttp3.OkHttpClient;
import org.bukkit.Server;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.ServicesManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scoreboard.ScoreboardManager;
import ru.voxile.battleleague.comboplugin.common.BeanFactory;
import ru.voxile.battleleague.comboplugin.spigot.BattleLeagueSpigotPlugin;

@RequiredArgsConstructor
public class SpigotBinderModule extends AbstractModule {

    private final BattleLeagueSpigotPlugin plugin;

    public Injector createInjector() {
        return Guice.createInjector(this);
    }

    @Override
    protected void configure() {
        // Экземпляр плагина.
        bind(Plugin.class).toInstance(plugin);
        bind(JavaPlugin.class).toInstance(plugin);
        bind(BattleLeagueSpigotPlugin.class).toInstance(plugin);

        // Различные серверные сервисы.
        Server server = plugin.getServer();
        bind(Server.class).toInstance(server);
        bind(BukkitScheduler.class).toInstance(server.getScheduler());
        bind(PluginManager.class).toInstance(server.getPluginManager());
        bind(ServicesManager.class).toInstance(server.getServicesManager());
        bind(ScoreboardManager.class).toInstance(server.getScoreboardManager());

        // Утилиты.
        bind(Gson.class).toInstance(BeanFactory.gson());
        bind(OkHttpClient.class).toInstance(BeanFactory.httpClient());
    }
}
