package ru.voxile.battleleague.comboplugin.common;

import lombok.experimental.UtilityClass;

@SuppressWarnings("unused")
@UtilityClass
public class Constants {

    public final long TICKS_PER_SECOND = 20L;

    public long toSeconds(long ticks) {
        return ticks / TICKS_PER_SECOND;
    }

    public long toTicks(long seconds) {
        return seconds * TICKS_PER_SECOND;
    }
}
