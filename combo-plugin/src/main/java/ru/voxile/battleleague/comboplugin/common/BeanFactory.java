package ru.voxile.battleleague.comboplugin.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.experimental.UtilityClass;
import okhttp3.OkHttpClient;

import java.time.Duration;

@UtilityClass
public class BeanFactory {

    public static Gson gson() {
        return new GsonBuilder()
                .setLenient()
                // .setPrettyPrinting()
                .create();
    }

    public static OkHttpClient httpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(Duration.ofSeconds(2))
                .readTimeout(Duration.ofSeconds(5))
                .callTimeout(Duration.ofSeconds(10))
                .build();
    }
}
