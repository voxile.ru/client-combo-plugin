package ru.voxile.battleleague.comboplugin.spigot.admin.localization;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Language {

    @UtilityClass
    public class Commands {

        public final String ACCESS_DENIED = "§cКоманда недоступна.";
        public final String PLAYER_ONLY_COMMAND = "§cКоманда только для игроков.";

        public final String[] HELP = {
                "§6/battleleague [help] §r- показать эту справку",
                "§6/battleleague list §r- список всех зон ожидания",
                "§6/battleleague create [id] §r- создание новой зоны ожидания",
                "§6/battleleague remove <id> §r- удаление зоны ожидания",
                "§6/battleleague reload §r- перезагрузка плагина, чтение конфигурации из файла.",
        };

        public final String PLUGIN_RELOADED = "§aПлагин перезагружен.";

        @UtilityClass
        public class Areas {

            public final String NO_AREAS = "§bНет существующих зон ожидания.";

            public final String PLEASE_LOOK_AT_BLOCK = "§cНеобходимо смотреть на угловой блок.";
            public final String SELECT_P2 = "§bТеперь посмотрите на блок в противоположном углу и повторите команду.";
            public final String CREATED = "§aЗона ожидания создана.";
            public final String NOT_CREATED = "§cЗона ожидания не создана.";

            public final String DELETED = "§aЗона ожидания удалена.";
            public final String NOT_DELETED = "§cЗона ожидания не удалена.";
        }
    }

    public final String NOT_IMPLEMENTED = "§cФункционал ещё не реализован.";
}
