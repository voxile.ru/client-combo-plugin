package ru.voxile.battleleague.comboplugin.spigot.messaging;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.RequiredArgsConstructor;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import ru.voxile.battleleague.comboplugin.common.JsonUtils;
import ru.voxile.battleleague.comboplugin.common.SpigotBungeeMessaging;
import ru.voxile.battleleague.comboplugin.common.dto.BungeeCommandDto;

@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class BungeeCordMessenger {

    private final Server server;
    private final Plugin plugin;
    private final PluginLogger logger;

    public void sendMessage(BungeeCommandDto dto) {
        try {
            logger.info("Sending %s command to BungeeCord.", dto.getAction().name());

            // TODO: Реализовать очередь отложенной отправки.

            Player player = server.getOnlinePlayers()
                    .stream()
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException("At least one online player is required."));

            player.sendPluginMessage(plugin,
                    SpigotBungeeMessaging.CHANNEL_ID,
                    JsonUtils.write(dto));
        } catch (Exception ex) {
            logger.error("Cannot send plugin message: %s", ex.getMessage());
        }
    }
}
