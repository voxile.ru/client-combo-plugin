package ru.voxile.battleleague.comboplugin.common.http.partners;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.RequiredArgsConstructor;
import ru.voxile.battleleague.comboplugin.api.broadcast.PullTargetedMessagesRequestDto;
import ru.voxile.battleleague.comboplugin.api.broadcast.PullTargetedMessagesResponseDto;
import ru.voxile.battleleague.comboplugin.api.play.PlayersReadyRequestDto;
import ru.voxile.battleleague.comboplugin.api.play.PlayersReadyRequestDto.GameVersion;
import ru.voxile.battleleague.comboplugin.api.play.PlayersReadyResponseDto;
import ru.voxile.battleleague.comboplugin.api.play.ProjectPlayerDto;
import ru.voxile.battleleague.comboplugin.common.http.HttpService;

import java.util.List;

@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class PartnerApiRestService {

    private final HttpService httpService;

    public PlayersReadyResponseDto sendAreaPlayers(
            String projectKey,
            GameVersion gameVersion,
            String areaName,
            List<ProjectPlayerDto> players
    ) {
        PlayersReadyRequestDto requestDto = PlayersReadyRequestDto.builder()
                .projectKey(projectKey)
                .gameVersion(gameVersion)
                .areaName(areaName)
                .players(players)
                .build();

        return httpService.post(
                PartnerApiMethods.ROOM_PLAYERS,
                requestDto,
                PlayersReadyResponseDto.class);
    }

    public PullTargetedMessagesResponseDto getNextMessage(String projectKey) {
        PullTargetedMessagesRequestDto requestDto = PullTargetedMessagesRequestDto.builder()
                .projectKey(projectKey)
                .build();

        return httpService.post(
                PartnerApiMethods.GET_MESSAGE,
                requestDto,
                PullTargetedMessagesResponseDto.class);
    }
}
