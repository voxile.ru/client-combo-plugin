package ru.voxile.battleleague.comboplugin.spigot.games.model;

import lombok.Value;
import one.util.streamex.StreamEx;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Value
public class Area {

    String name;
    Location min;
    Location max;

    Map<String, String> properties;

    public Collection<Player> whichPlayersInside(Collection<Player> players) {
        return StreamEx.of(players)
                .filter(OfflinePlayer::isOnline)
                .mapToEntry(Entity::getLocation)
                .filterValues(this::contains)
                .keys()
                .toList();
    }

    private boolean contains(Location location) {
        return Objects.equals(location.getWorld(), min.getWorld())
               && isBetween(min.getBlockX(), location.getBlockX(), max.getBlockX())
               && isBetween(min.getBlockY(), location.getBlockY(), max.getBlockY())
               && isBetween(min.getBlockZ(), location.getBlockZ(), max.getBlockZ());
    }

    private static boolean isBetween(int min, int pos, int max) {
        return min <= pos && pos <= max;
    }

    public String toPrettyString() {
        return String.format("%s @ %s ([%d; %d; %d] - [%d; %d; %d])",
                name,
                min.getWorld().getName(),
                min.getBlockX(), min.getBlockY(), min.getBlockZ(),
                max.getBlockX(), max.getBlockY(), max.getBlockZ());
    }

    public static Area create(String name, Location p1, Location p2) {
        return create(name, p1, p2, new HashMap<>());
    }

    public static Area create(String name, Location p1, Location p2, Map<String, String> properties) {
        if (name == null || name.isEmpty()
            || p1 == null || p2 == null
            || !p1.getWorld().equals(p2.getWorld())) {
            // Локации не заданы или находятся в разных мирах.
            return null;
        }

        if (isP1LessP2(p1, p2)) {
            return new Area(name, p1, p2, properties);
        }

        World world = p1.getWorld();
        Location min = new Location(world,
                Math.min(p1.getBlockX(), p2.getBlockX()),
                Math.min(p1.getBlockY(), p2.getBlockY()),
                Math.min(p1.getBlockZ(), p2.getBlockZ()));
        Location max = new Location(world,
                Math.max(p1.getBlockX(), p2.getBlockX()),
                Math.max(p1.getBlockY(), p2.getBlockY()),
                Math.max(p1.getBlockZ(), p2.getBlockZ()));
        return new Area(name, min, max, properties);
    }

    private static boolean isP1LessP2(Location p1, Location p2) {
        return p1.getX() <= p2.getX()
               && p1.getY() <= p2.getY()
               && p1.getZ() <= p2.getZ();
    }
}
