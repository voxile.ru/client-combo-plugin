package ru.voxile.battleleague.comboplugin.spigot.messaging;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;
import ru.voxile.battleleague.comboplugin.api.broadcast.PullTargetedMessagesResponseDto;
import ru.voxile.battleleague.comboplugin.common.Constants;
import ru.voxile.battleleague.comboplugin.spigot.http.PartnerApiService;

import java.util.Collection;
import java.util.function.Consumer;

@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class MessagePuller implements Runnable {

    public static final long RUN_DELAY = Constants.toTicks(30);
    public static final long RUN_PERIOD = Constants.toTicks(5);

    private final Server server;
    private final Plugin plugin;
    private final BukkitScheduler scheduler;
    private final PluginLogger logger;
    private final Gson gson;

    private final PartnerApiService partnerApiService;

    @Override
    public void run() {
        Collection<? extends Player> onlinePlayers = server.getOnlinePlayers();
        int onlinePlayersCount = onlinePlayers.size();
        if (onlinePlayersCount == 0) {
            // Если некому показывать сообщения, то пропускаем цикл.
            return;
        }

        try {
            partnerApiService
                    .getNextMessage()
                    .ifPresent(this::broadcast);
        } catch (Exception ex) {
            logger.warn("Cannot fetch and broadcast message: %s", ex.getMessage());
        }
    }

    private void broadcast(PullTargetedMessagesResponseDto message) {
        Consumer<Player> sender;
        switch (message.getFormat()) {
            case LEGACY:
                String plainText = message.getMessage();
                sender = p -> p.sendMessage(plainText);
                break;
            case JSON:
                String jsonPayload = message.getMessage();
                BaseComponent[] components = ComponentSerializer.parse(jsonPayload);
                sender = p -> p.spigot().sendMessage(components);
                break;
            default:
                logger.warn("Unsupported message format: ", message.getFormat());
                return;
        }

        scheduler.runTask(plugin, () -> {
            for (Player player : server.getOnlinePlayers()) {
                sender.accept(player);
            }
        });
    }
}
