package ru.voxile.battleleague.comboplugin.spigot.utils;

import lombok.experimental.UtilityClass;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.Optional;

@UtilityClass
public class PlayerUtils {

    public Location getBlockLookingAt(Player player) {
        return Optional.ofNullable(player)
                .map(p -> p.getTargetBlock(null, 100))
                .map(Block::getLocation)
                .orElse(null);
    }
}
