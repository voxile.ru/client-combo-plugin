package ru.voxile.battleleague.comboplugin.bungee.implementation;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.Connection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.ArrayUtils;
import ru.voxile.battleleague.comboplugin.common.Permissions;

import java.util.Collection;
import java.util.List;

@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class AdminMessenger {

    private final Plugin plugin;

    public void messageToBungeeCordAdmins(@NonNull String format, Object... args) {
        String message = ArrayUtils.isNotEmpty(args)
                ? String.format(format, args)
                : format;
        TextComponent textComponent = new TextComponent(message);

        List<ProxiedPlayer> recipients = getBungeeCordOnlineAdmins();
        recipients.forEach(p -> p.sendMessage(textComponent));
    }

    private List<ProxiedPlayer> getBungeeCordOnlineAdmins() {
        ProxyServer proxy = plugin.getProxy();

        Collection<ProxiedPlayer> proxiedPlayers = proxy.getPlayers();

        return StreamEx.of(proxiedPlayers)
                .filter(Connection::isConnected)
                .mapToEntry(CommandSender::getPermissions)
                .filterValues(p -> p != null
                                   && p.contains(Permissions.SINGLE_GLOBAL_PERMISSION))
                .keys()
                .toList();
    }
}
