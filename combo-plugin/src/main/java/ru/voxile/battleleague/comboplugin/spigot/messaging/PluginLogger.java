package ru.voxile.battleleague.comboplugin.spigot.messaging;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.bukkit.plugin.Plugin;

@SuppressWarnings("unused")
@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class PluginLogger {

    private final Plugin plugin;

    public void debug(String message) {
        plugin.getLogger().fine(message);
    }

    public void debug(@NonNull String format, Object... args) {
        plugin.getLogger().fine(String.format(format, args));
    }

    public void info(String message) {
        plugin.getLogger().info(message);
    }

    public void info(@NonNull String format, Object... args) {
        plugin.getLogger().info(String.format(format, args));
    }

    public void warn(String message) {
        plugin.getLogger().warning(message);
    }

    public void warn(@NonNull String format, Object... args) {
        plugin.getLogger().warning(String.format(format, args));
    }

    public void error(String message) {
        plugin.getLogger().severe(message);
    }

    public void error(@NonNull String format, Object... args) {
        plugin.getLogger().severe(String.format(format, args));
    }
}
