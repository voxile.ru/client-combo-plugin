package ru.voxile.battleleague.comboplugin.spigot.games;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.RequiredArgsConstructor;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import ru.voxile.battleleague.comboplugin.api.play.PlayersReadyResponseDto;
import ru.voxile.battleleague.comboplugin.common.Constants;
import ru.voxile.battleleague.comboplugin.common.http.exceptions.HttpInvocationException;
import ru.voxile.battleleague.comboplugin.spigot.games.model.Area;
import ru.voxile.battleleague.comboplugin.spigot.http.PartnerApiService;

import java.util.Collection;
import java.util.HashSet;

@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class BackgroundProcessor implements Runnable {

    public static final long RUN_DELAY = Constants.toTicks(15);
    public static final long RUN_PERIOD = Constants.toTicks(5);

    private final Server server;
    private final AreaService areaService;
    private final GameService gameService;
    private final PartnerApiService partnerApiService;

    @Override
    public void run() {
        dispatchPlayers(new HashSet<>(server.getOnlinePlayers()));
    }

    private void dispatchPlayers(Collection<Player> players) {
        Collection<Area> areas = areaService.getAll();
        areas.forEach(a -> dispatchArea(a, players));
    }

    private void dispatchArea(Area area, Collection<Player> players) {
        try {
            Collection<Player> playersInsideArea = area.whichPlayersInside(players);
            PlayersReadyResponseDto coordinatorResponse = partnerApiService
                    .sendAreaPlayers(area, playersInsideArea);
            gameService.onCoordinatorResponse(area, coordinatorResponse);
        } catch (HttpInvocationException ignored) {
            // Запрос не выполнился, попробуем позже ещё раз.
        }
    }
}
