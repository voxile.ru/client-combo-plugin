package ru.voxile.battleleague.comboplugin.common.dto;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class BungeeCommandDto {

    @NonNull
    GameServerInfo serverInfo;

    @NonNull
    BungeeCommandAction action;

    public enum BungeeCommandAction {

        ADD_SERVER,

        REMOVE_SERVER,
    }
}
