package ru.voxile.battleleague.comboplugin.spigot.http;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.entity.Player;
import ru.voxile.battleleague.comboplugin.api.broadcast.PullTargetedMessagesResponseDto;
import ru.voxile.battleleague.comboplugin.api.play.PlayersReadyResponseDto;
import ru.voxile.battleleague.comboplugin.api.play.ProjectPlayerDto;
import ru.voxile.battleleague.comboplugin.common.http.partners.PartnerApiRestService;
import ru.voxile.battleleague.comboplugin.spigot.BattleLeagueSpigotPlugin;
import ru.voxile.battleleague.comboplugin.spigot.games.model.Area;
import ru.voxile.battleleague.comboplugin.spigot.games.model.AreaProperties;
import ru.voxile.battleleague.comboplugin.spigot.messaging.PluginLogger;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class PartnerApiService {

    private final BattleLeagueSpigotPlugin plugin;
    private final PluginLogger logger;
    private final PlayerMapper playerMapper;

    private final PartnerApiRestService partnerApiRestService;

    public PlayersReadyResponseDto sendAreaPlayers(
            @NonNull Area area,
            @NonNull Collection<Player> playersInsideArea
    ) {
        logger.debug("Coordinator API sendAreaPlayers() request.");
        List<ProjectPlayerDto> players = StreamEx.of(playersInsideArea)
                .map(playerMapper::toProjectPlayerDto)
                .toList();

        String projectKey = Optional.of(area)
                .map(Area::getProperties)
                .map(p -> p.get(AreaProperties.AREA_PROJECT_KEY))
                .filter(apk -> !apk.isEmpty())
                .orElseGet(plugin::getProjectKey);

        PlayersReadyResponseDto responseDto = partnerApiRestService.sendAreaPlayers(projectKey, plugin.getGameVersion(), area.getName(), players);
        logger.debug("Coordinator API sendAreaPlayers() response: %s", responseDto);

        return responseDto;
    }

    public Optional<PullTargetedMessagesResponseDto> getNextMessage() {
        logger.debug("Coordinator API getNextMessage() request.");

        String projectKey = plugin.getProjectKey();
        PullTargetedMessagesResponseDto responseDto = partnerApiRestService.getNextMessage(projectKey);

        logger.debug("Coordinator API getNextMessage() response: %s", responseDto);
        return Optional.ofNullable(responseDto)
                .filter(m -> StringUtils.isNotBlank(m.getMessage()))
                .filter(m -> m.getFormat() != null);
    }
}
