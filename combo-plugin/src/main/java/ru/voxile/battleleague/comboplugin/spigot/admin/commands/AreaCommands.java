package ru.voxile.battleleague.comboplugin.spigot.admin.commands;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import ru.voxile.battleleague.comboplugin.common.Permissions;
import ru.voxile.battleleague.comboplugin.spigot.BattleLeagueSpigotPlugin;
import ru.voxile.battleleague.comboplugin.spigot.admin.localization.Language;
import ru.voxile.battleleague.comboplugin.spigot.games.AreaService;
import ru.voxile.battleleague.comboplugin.spigot.games.model.Area;
import ru.voxile.battleleague.comboplugin.spigot.messaging.PluginLogger;
import ru.voxile.battleleague.comboplugin.spigot.utils.PlayerUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.function.BiConsumer;

@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class AreaCommands implements CommandExecutor {

    public static final String SINGLE_GLOBAL_COMMAND = "battleleague";

    private final BattleLeagueSpigotPlugin plugin;
    private final PluginLogger logger;
    private final AreaService areaService;

    private final Map<UUID, CreateAreaContext> createAreaContextPerPlayerUniqueId = new HashMap<>();

    public final Map<String, BiConsumer<CommandSender, List<String>>> processors = EntryStream
            .<String, BiConsumer<CommandSender, List<String>>>empty()
            .append("reload", this::reloadPlugin)
            .append("list", this::listAreas)
            .append("create", this::createArea)
            .append("remove", this::removeArea)
            .append("help", AreaCommands::showHelp)
            .toImmutableMap();

    @Override
    public boolean onCommand(
            @NotNull CommandSender sender,
            @NotNull Command command,
            @NotNull String label,
            @NotNull String[] args
    ) {
        try {
            // Как мы сюда вообще попали?
            if (!SINGLE_GLOBAL_COMMAND.equals(command.getName())) {
                return false;
            }

            // Ты кто такой, давай до свидания!
            if (!sender.hasPermission(Permissions.SINGLE_GLOBAL_PERMISSION)) {
                sender.sendMessage(Language.Commands.ACCESS_DENIED);
                return true;
            }

            processHubCommands(sender, args);
            return true;
        } catch (Exception ex) {
            logger.error("Exception in command processing: %s.", ex.getMessage());
            return true;
        }
    }

    private void processHubCommands(
            @NonNull CommandSender sender,
            @NonNull String[] args
    ) {
        String subcommand = ArrayUtils.isNotEmpty(args)
                ? args[0].toLowerCase()
                : "help";
        BiConsumer<CommandSender, List<String>> processor = processors
                .getOrDefault(subcommand, AreaCommands::showHelp);

        List<String> subArgs = Arrays.asList(args).subList(1, args.length);
        processor.accept(sender, subArgs);
    }

    private void reloadPlugin(CommandSender sender, List<String> args) {
        plugin.reload();
        sender.sendMessage(Language.Commands.PLUGIN_RELOADED);
    }

    private void listAreas(CommandSender sender, List<String> args) {
        Collection<Area> areas = areaService.getAll();
        StreamEx.of(areas)
                .map(Area::toPrettyString)
                .ifEmpty(Language.Commands.Areas.NO_AREAS)
                .forEach(sender::sendMessage);
    }

    private void createArea(CommandSender sender, List<String> args) {
        if (sender instanceof ConsoleCommandSender) {
            sender.sendMessage(Language.Commands.PLAYER_ONLY_COMMAND);
            return;
        }

        Player player = (Player) sender;
        UUID uniqueId = player.getUniqueId();

        String areaName = args.isEmpty()
                ? null
                : args.get(0);

        CreateAreaContext context = createAreaContextPerPlayerUniqueId.get(uniqueId);
        if (context == null) {
            createAreaStep1(player, areaName);
            return;
        }

        createAreaStep2(context, player, areaName);
    }

    private void createAreaStep1(
            @NonNull Player player,
            String areaName
    ) {
        Location currentLocation = PlayerUtils.getBlockLookingAt(player);
        if (currentLocation == null) {
            player.sendMessage(Language.Commands.Areas.PLEASE_LOOK_AT_BLOCK);
            return;
        }

        CreateAreaContext context = new CreateAreaContext();
        context.setFirstPoint(currentLocation);
        context.setName(areaName);

        createAreaContextPerPlayerUniqueId.put(player.getUniqueId(), context);

        player.sendMessage(Language.Commands.Areas.SELECT_P2);
    }

    private void createAreaStep2(
            @NonNull CreateAreaContext context,
            @NonNull Player player,
            String areaName
    ) {
        Location cachedLocation = context.getFirstPoint();
        Location currentLocation = PlayerUtils.getBlockLookingAt(player);
        if (currentLocation == null) {
            player.sendMessage(Language.Commands.Areas.PLEASE_LOOK_AT_BLOCK);
            return;
        }
        if (!Objects.equals(cachedLocation.getWorld(), cachedLocation.getWorld())) {
            createAreaStep1(player, areaName);
            return;
        }

        Area area = areaService.register(areaName, currentLocation, cachedLocation);
        createAreaContextPerPlayerUniqueId.remove(player.getUniqueId());

        boolean created = area != null;
        String message = created
                ? Language.Commands.Areas.CREATED
                : Language.Commands.Areas.NOT_CREATED;
        player.sendMessage(message);
    }

    private void removeArea(CommandSender sender, List<String> args) {
        if (!args.isEmpty()) {
            String areaName = args.get(0);
            Area area = areaService.getByName(areaName);
            if (area != null) {
                areaService.deleteArea(area);
                sender.sendMessage(Language.Commands.Areas.DELETED);
                return;
            }
        }

        sender.sendMessage(Language.Commands.Areas.NOT_DELETED);
    }

    private static void showHelp(CommandSender sender, List<String> args) {
        sender.sendMessage(Language.Commands.HELP);
    }

    @Data
    public static class CreateAreaContext {

        private Location firstPoint;
        private String name;
    }
}
