package ru.voxile.battleleague.comboplugin.common.http;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.voxile.battleleague.comboplugin.common.http.exceptions.HttpInvocationException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Optional;

@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class HttpService {

    private static final MediaType APPLICATION_JSON_UTF8 = MediaType.parse("application/json; charset=utf8");

    private static final String BASE_URL = getBaseUrl();

    private final Gson gson;
    private final OkHttpClient okHttpClient;

    /**
     * @throws HttpInvocationException В случаях сетевых сбоев или при ответах статусами 4xx и 5xx.
     */
    @SneakyThrows
    public <R, T> R get(
            @NonNull String uri,
            @NonNull Class<R> receiveClass
    ) {
        Request request = new Request.Builder()
                .get()
                .url(BASE_URL + uri)
                .build();
        return exchange(request, receiveClass);
    }

    /**
     * @throws HttpInvocationException В случаях сетевых сбоев или при ответах статусами 4xx и 5xx.
     */
    @SneakyThrows
    public <R, T> R post(
            @NonNull String uri,
            @NonNull T transmitObject,
            @NonNull Class<R> receiveClass
    ) {
        String requestData = gson.toJson(transmitObject);
        RequestBody requestBody = RequestBody.create(requestData, APPLICATION_JSON_UTF8);
        Request request = new Request.Builder()
                .post(requestBody)
                .url(BASE_URL + uri)
                .build();
        return exchange(request, receiveClass);
    }

    private <R> R exchange(
            @NonNull Request request,
            @NonNull Class<R> receiveClass
    ) {
        try (Response response = okHttpClient.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                String message = String.format("Problem calling coordinator API (status %d): %s",
                        response.code(), response.message());
                throw HttpInvocationException.builder()
                        .message(message)
                        .build();
            }

            return parseResponse(response, receiveClass);
        } catch (IOException ex) {
            throw HttpInvocationException.builder()
                    .message("Exception during API call to coordinator: " + ex.getMessage())
                    .cause(ex)
                    .build();
        } catch (JsonSyntaxException ex) {
            throw HttpInvocationException.builder()
                    .message("Response from coordinator is not a parsable JSON.")
                    .cause(ex)
                    .build();
        }
    }

    @Nullable
    @SneakyThrows
    private <R> R parseResponse(Response response, @NotNull Class<R> clazz) {
        if (clazz.equals(Void.class)) {
            return null;
        }
        ResponseBody responseBody = response.body();
        if (responseBody == null) {
            return null;
        }

        String responseData = responseBody.string();
        if (clazz.equals(String.class)) {
            //noinspection unchecked
            return (R) responseData;
        }

        return gson.fromJson(responseData, clazz);
    }

    private static String getBaseUrl() {
        return Optional.of("COORDINATOR_URL")
                .map(System::getenv)
                .filter(e -> !e.isEmpty())
                .orElseGet(() -> new String(
                        Base64.getDecoder().decode("aHR0cHM6Ly9hcGkuYmF0dGxlLWxlYWd1ZS5ydQ=="),
                        StandardCharsets.UTF_8));
    }
}
