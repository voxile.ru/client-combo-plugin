package ru.voxile.battleleague.comboplugin.spigot.games.model;

import lombok.experimental.UtilityClass;

@UtilityClass
public class AreaProperties {

    public final String AREA_PROJECT_KEY = "projectKey";
}
