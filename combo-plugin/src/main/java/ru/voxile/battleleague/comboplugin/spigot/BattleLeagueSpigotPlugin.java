package ru.voxile.battleleague.comboplugin.spigot;

import com.google.inject.Inject;
import com.google.inject.Injector;
import lombok.Getter;
import org.bukkit.Server;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.Messenger;
import org.bukkit.scheduler.BukkitScheduler;
import ru.voxile.battleleague.comboplugin.api.play.PlayersReadyRequestDto.GameVersion;
import ru.voxile.battleleague.comboplugin.common.SpigotBungeeMessaging;
import ru.voxile.battleleague.comboplugin.common.http.HttpService;
import ru.voxile.battleleague.comboplugin.spigot.admin.commands.AreaCommands;
import ru.voxile.battleleague.comboplugin.spigot.di.SpigotBinderModule;
import ru.voxile.battleleague.comboplugin.spigot.games.AreaService;
import ru.voxile.battleleague.comboplugin.spigot.games.BackgroundProcessor;
import ru.voxile.battleleague.comboplugin.spigot.messaging.BungeeCordMessenger;
import ru.voxile.battleleague.comboplugin.spigot.messaging.MessagePuller;

import java.util.Objects;

public final class BattleLeagueSpigotPlugin extends JavaPlugin {

    /**
     * Секретный ключ данного проекта.
     */
    @Getter
    private String projectKey;

    @Getter
    @Inject
    private HttpService httpService;
    @Getter
    @Inject
    private BungeeCordMessenger bungeeCordMessenger;
    @Inject
    private BackgroundProcessor backgroundProcessor;
    @Inject
    private MessagePuller messagePuller;
    @Inject
    private AreaService areaService;
    @Inject
    private AreaCommands areaCommands;

    private GameVersion gameVersion;

    @Override
    public void onEnable() {
        // Подготовка конфигурации.
        saveDefaultConfig();
        reloadConfig();
        this.projectKey = Objects.requireNonNull(
                getConfig().getString("general.project-key"),
                "Value general.project-key is not set.");

        // Dependency Injection.
        SpigotBinderModule module = new SpigotBinderModule(this);
        Injector injector = module.createInjector();
        injector.injectMembers(this);

        // Восстановление состояния.
        areaService.reloadFromConfig();

        // Регистрация компонентов.
        Server server = getServer();

        PluginCommand commandHub = server.getPluginCommand(AreaCommands.SINGLE_GLOBAL_COMMAND);
        commandHub.setExecutor(areaCommands);

        BukkitScheduler scheduler = server.getScheduler();
        scheduler.runTaskTimerAsynchronously(
                this,
                backgroundProcessor,
                BackgroundProcessor.RUN_DELAY,
                BackgroundProcessor.RUN_PERIOD);
        scheduler.runTaskTimerAsynchronously(
                this,
                messagePuller,
                MessagePuller.RUN_DELAY,
                MessagePuller.RUN_PERIOD);

        Messenger messenger = server.getMessenger();
        messenger.registerOutgoingPluginChannel(this, SpigotBungeeMessaging.CHANNEL_ID);
    }

    @Override
    public void onDisable() {
        getServer().getServicesManager().unregisterAll(this);
        getServer().getScheduler().cancelTasks(this);
    }

    public void reload() {
        PluginManager pluginManager = getServer().getPluginManager();
        pluginManager.disablePlugin(this);
        pluginManager.enablePlugin(this);
    }

    public GameVersion getGameVersion() {
        if (gameVersion != null) {
            return gameVersion;
        }

        Server server = getServer();
        GameVersion result = GameVersion.builder()
                .minecraftVersion(server.getVersion())
                .build();
        gameVersion = result;
        return result;
    }
}
