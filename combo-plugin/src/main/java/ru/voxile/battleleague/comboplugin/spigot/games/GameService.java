package ru.voxile.battleleague.comboplugin.spigot.games;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import ru.voxile.battleleague.comboplugin.api.common.RemoteServerAddress;
import ru.voxile.battleleague.comboplugin.api.common.RoomStatus;
import ru.voxile.battleleague.comboplugin.api.play.PlayersReadyResponseDto;
import ru.voxile.battleleague.comboplugin.common.dto.BungeeCommandDto;
import ru.voxile.battleleague.comboplugin.common.dto.GameServerInfo;
import ru.voxile.battleleague.comboplugin.spigot.games.model.Area;
import ru.voxile.battleleague.comboplugin.spigot.games.model.GameContext;
import ru.voxile.battleleague.comboplugin.spigot.messaging.BungeeCordMessenger;
import ru.voxile.battleleague.comboplugin.spigot.messaging.PlayersMessenger;
import ru.voxile.battleleague.comboplugin.spigot.messaging.PluginLogger;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class GameService {

    private final PluginLogger logger;
    private final PlayersMessenger playersMessenger;
    private final BungeeCordMessenger bungeeCordMessenger;

    private final Map<String, GameContext> contexts = new ConcurrentHashMap<>();

    public void onCoordinatorResponse(Area area, PlayersReadyResponseDto responseDto) {
        GameContext context = getAreaContext(area);
        RoomStatus prevRoomStatus = context.getRoomStatus();

        Collection<UUID> players = responseDto.getPlayers();
        RoomStatus currRoomStatus = responseDto.getRoomStatus();

        context.setPlayers(players);
        context.setRoomId(responseDto.getRoomId());
        context.setRoomStatus(currRoomStatus);

        if (prevRoomStatus != currRoomStatus) {
            logger.info("Changing area '%s' status from %s to %s.",
                    area.getName(), prevRoomStatus, currRoomStatus);
        }

        // Обрабатываем значение адреса удалённого игрового сервера.
        processServerAddress(context, responseDto.getServerAddress(), players);

        processRoomStatus(context, responseDto, prevRoomStatus, currRoomStatus, players);
    }

    private void processRoomStatus(
            GameContext context,
            PlayersReadyResponseDto responseDto,
            RoomStatus previousStatus,
            RoomStatus currentStatus,
            Collection<UUID> players
    ) {
        String areaName = context.getArea().getName();

        String prevRoomId = context.getRoomId();
        String currRoomId = responseDto.getRoomId();
        if (!Objects.equals(prevRoomId, currRoomId)) {
            //noinspection VariableNotUsedInsideIf
            String message = currRoomId != null
                    ? String.format("§dВ зоне ожидания §a%s§d запланирована игра.", areaName)
                    : String.format("§dВ зоне ожидания §a%s§d закончилась игра.", areaName);
            playersMessenger.broadcastToAll(message);
        }

        // Обрабатываем только переходы между статусами игры.
        if (previousStatus == currentStatus) {
            return;
        }

        String statusCaption = currentStatus.getCaption();
        String advertisement = String.format("§dСтатус зоны ожидания §a%s§d теперь '§6%s§d'.", areaName, statusCaption);
        playersMessenger.broadcastToAll(advertisement);

        switch (currentStatus) {
            case WAITING:
                // Ничего не происходит, ожидается сбор достаточного количества игроков в этой зоне или
                // ожидание готовности игроков команд-соперников.
                break;
            case STARTING:
            case RUNNING:
                // Команды собраны, производится запуск игрового сервера.
                // Возможно, игра уже идёт.
                context.setPlayers(players);
                context.setRoomId(responseDto.getRoomId());
                break;
            case STOPPING:
                // Игра завершается, игроки скоро будут перемещены обратно.
                break;
            case FINISHED:
                // Игра полностью закончена, сервер остановлен.
                break;
        }
    }

    private void processServerAddress(GameContext context, RemoteServerAddress currAddress, Collection<UUID> players) {
        RemoteServerAddress prevAddress = context.getAddress();
        if (prevAddress == null && currAddress == null) {
            // Игрового сервера как не было, так и нет.
            return;
        }

        if (prevAddress != null && prevAddress.equals(currAddress)) {
            // Игровой сервер как был, так и есть.
            sendPlayersToServer(context.getArea().getName(), currAddress, players);
            return;
        }

        // Новое значение отлично от старого, обновляем его в контексте
        // и совершаем нужное действие.
        context.setAddress(currAddress);

        if (currAddress == null) {
            // Игровой сервер был, а теперь его нет.
            removePlayersFromServer(context.getArea().getName(), prevAddress);
            return;
        }

        // Сервера не было, но он появился, значит игроков можно
        // телепортировать на него.
        sendPlayersToServer(context.getArea().getName(), currAddress, players);
    }

    private GameContext getAreaContext(Area area) {
        return contexts.computeIfAbsent(area.getName(), an -> newGameContext(area));
    }

    private static GameContext newGameContext(Area area) {
        GameContext result = new GameContext();
        result.setArea(area);
        result.setPlayers(Collections.emptyList());
        result.setRoomId(null);
        result.setRoomStatus(RoomStatus.WAITING);
        return result;
    }

    private void sendPlayersToServer(@NonNull String areaName, @NonNull RemoteServerAddress serverAddress, Collection<UUID> players) {
        GameServerInfo gameServerInfo = GameServerInfo.builder()
                .areaName(areaName)
                .host(serverAddress.getHost())
                .port(serverAddress.getPort())
                .playerIds(players)
                .forceGloballyUniqueName(true)
                .build();
        BungeeCommandDto bungeeCommandDto = BungeeCommandDto.builder()
                .action(BungeeCommandDto.BungeeCommandAction.ADD_SERVER)
                .serverInfo(gameServerInfo)
                .build();
        bungeeCordMessenger.sendMessage(bungeeCommandDto);
    }

    private void removePlayersFromServer(@NonNull String areaName, @NonNull RemoteServerAddress serverAddress) {
        GameServerInfo gameServerInfo = GameServerInfo.builder()
                .areaName(areaName)
                .host(serverAddress.getHost())
                .port(serverAddress.getPort())
                .playerIds(Collections.emptyList())
                .forceGloballyUniqueName(true)
                .build();
        BungeeCommandDto bungeeCommandDto = BungeeCommandDto.builder()
                .action(BungeeCommandDto.BungeeCommandAction.REMOVE_SERVER)
                .serverInfo(gameServerInfo)
                .build();
        bungeeCordMessenger.sendMessage(bungeeCommandDto);
    }
}
