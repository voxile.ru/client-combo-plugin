package ru.voxile.battleleague.comboplugin.bungee.implementation;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;
import one.util.streamex.EntryStream;
import org.apache.commons.lang3.StringUtils;
import ru.voxile.battleleague.comboplugin.common.dto.GameServerInfo;

import java.net.InetSocketAddress;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

@Singleton
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class ServersContainer {

    private final Map<String, GameServerContext> currentServers = new ConcurrentHashMap<>();

    private final Plugin plugin;
    private final AdminMessenger adminMessenger;

    public boolean isRegisteredNew(@NonNull GameServerInfo remoteServer) {
        String serverName = remoteServer.toUniqueName();

        AtomicBoolean created = new AtomicBoolean();

        currentServers.computeIfAbsent(
                serverName,
                k -> {
                    created.set(true);
                    return createGameContext(serverName, remoteServer);
                });

        return created.get();
    }

    private GameServerContext createGameContext(
            @NotNull String serverName,
            @NotNull GameServerInfo remoteServer
    ) {
        ProxyServer proxy = plugin.getProxy();
        Map<String, ServerInfo> registeredServers = proxy.getServers();

        ServerInfo registration = registeredServers.computeIfAbsent(
                serverName,
                k -> registerServer(serverName, remoteServer));

        GameServerContext gameServerContext = new GameServerContext();
        gameServerContext.setDescription(remoteServer);
        gameServerContext.setRegistration(registration);
        return gameServerContext;
    }

    private ServerInfo registerServer(
            @NotNull String serverName,
            @NotNull GameServerInfo remoteServer
    ) {
        ProxyServer proxy = plugin.getProxy();
        InetSocketAddress address = InetSocketAddress.createUnresolved(
                remoteServer.getHost(),
                remoteServer.getPort());
        String messageOfTheDay = "Battle server " + StringUtils.left(serverName, 8);

        plugin.getLogger().fine(String.format("Registered server '%s' for lobby area '%s'.",
                serverName,
                remoteServer.getAreaName()));

        ServerInfo serverInfo = proxy.constructServerInfo(serverName, address, messageOfTheDay, remoteServer.isRestricted());

        adminMessenger.messageToBungeeCordAdmins("На прокси добавлен новый сервер: %s", serverInfo.getName());

        return serverInfo;
    }

    public Optional<ServerInfo> getRegistration(@NonNull GameServerInfo remoteServer) {
        return Optional.of(remoteServer)
                .map(GameServerInfo::toUniqueName)
                .flatMap(this::getRegistration);
    }

    public Optional<ServerInfo> getRegistration(@NonNull String serverName) {
        return Optional.of(serverName)
                .map(currentServers::get)
                .map(GameServerContext::getRegistration);
    }

    public Optional<ServerInfo> findGameForPlayer(UUID uniqueId) {
        return EntryStream.of(currentServers)
                .values()
                .mapToEntry(
                        GameServerContext::getDescription,
                        GameServerContext::getRegistration)
                .mapKeys(GameServerInfo::getPlayerIds)
                .filterKeys(uuids -> uuids.contains(uniqueId))
                .values()
                .findFirst();
    }

    public void unregister(@NonNull GameServerInfo remoteServer) {
        String serverName = remoteServer.toUniqueName();

        // Удалили контекст из списка текущих игр.
        currentServers.remove(serverName);

        // Удалили из списка серверов.
        ProxyServer proxy = plugin.getProxy();
        Map<String, ServerInfo> proxiedServers = proxy.getServers();
        ServerInfo serverInfo = proxiedServers.remove(serverName);

        if (serverInfo != null) {
            adminMessenger.messageToBungeeCordAdmins("На прокси удалён сервер: %s", serverInfo.getName());
        }

        plugin.getLogger().fine(String.format("Unregistered server '%s'.", serverName));
    }

    @Data
    public static class GameServerContext {

        private GameServerInfo description;
        private ServerInfo registration;
    }
}
