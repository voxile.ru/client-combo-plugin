package ru.voxile.battleleague.comboplugin.api.common;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class RemoteServerAddress {

    String host;

    int port;
}
