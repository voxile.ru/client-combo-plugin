package ru.voxile.battleleague.comboplugin.api.broadcast;

/**
 * Формат содержимого сообщения.
 */
public enum MessageFormat {

    /**
     * Старый формат.
     * Текст с цветовыми префиксами, например: "§aВсем привет!".
     */
    LEGACY,

    /**
     * Формат JSON с дополнительными возможностями форматирования.
     * Например:
     * <pre>
     * { "color": "green", "text": "Всем привет!" }
     * </pre>
     */
    JSON,
    ;
}
