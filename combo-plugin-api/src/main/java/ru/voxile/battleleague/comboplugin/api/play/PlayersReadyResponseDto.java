package ru.voxile.battleleague.comboplugin.api.play;

import lombok.Builder;
import lombok.Value;
import ru.voxile.battleleague.comboplugin.api.common.RemoteServerAddress;
import ru.voxile.battleleague.comboplugin.api.common.RoomStatus;

import java.util.Collection;
import java.util.UUID;

@Value
@Builder
public class PlayersReadyResponseDto {

    /**
     * Уникальный ID комнаты, в которой собрались ожидающие игроки.
     */
    String roomId;

    /**
     * Статус комнаты.
     */
    RoomStatus roomStatus;

    /**
     * Игроки, принятые в игру.
     */
    Collection<UUID> players;

    /**
     * Информация о сервере, на котором будет проводится игра.
     */
    RemoteServerAddress serverAddress;
}
