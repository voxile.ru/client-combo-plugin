package ru.voxile.battleleague.comboplugin.api.broadcast;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PullTargetedMessagesRequestDto {

    /**
     * Секретный ключ данного проекта.
     */
    @NotNull
    String projectKey;
}
