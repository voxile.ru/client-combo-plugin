package ru.voxile.battleleague.comboplugin.api.play;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class PlayersReadyRequestDto {

    /**
     * Секретный ключ данного проекта.
     */
    @NotNull
    String projectKey;

    /**
     * Название зоны, в которой собрались ожидающие игроки.
     */
    @NotEmpty
    String areaName;

    /**
     * Версия игры для более точного подбора сервера и соперников.
     */
    GameVersion gameVersion;

    /**
     * Ожидающие игру игроки.
     */
    @NotNull
    List<@Valid ProjectPlayerDto> players;

    @Value
    @Builder
    public static class GameVersion {

        @NotBlank
        String minecraftVersion;
    }
}
