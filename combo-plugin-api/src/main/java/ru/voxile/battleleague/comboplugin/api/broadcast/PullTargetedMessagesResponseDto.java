package ru.voxile.battleleague.comboplugin.api.broadcast;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PullTargetedMessagesResponseDto {

    public static final PullTargetedMessagesResponseDto EMPTY = PullTargetedMessagesResponseDto.builder().build();

    /**
     * Текст сообщения для отображения.
     */
    @NotBlank
    String message;

    /**
     * Способ форматирования сообщения.
     */
    @NotNull
    MessageFormat format;
}
