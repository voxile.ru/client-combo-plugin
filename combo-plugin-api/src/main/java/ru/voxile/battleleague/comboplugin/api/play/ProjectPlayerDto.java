package ru.voxile.battleleague.comboplugin.api.play;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Value;

import java.util.UUID;

@Value
@Builder
public class ProjectPlayerDto {

    /**
     * Уникальный идентификатор игрока.
     */
    @NotNull
    UUID playerUuid;

    /**
     * Логин игрока.
     */
    @NotBlank
    String playerName;
}
